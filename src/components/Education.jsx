import {
  Container,
  Flex,
  VStack,
  List,
  ListItem,
  ListIcon,
  Box,
} from "@chakra-ui/react";
import HeaderSection from "./HeaderSection";
import ItemTimeLine from "./ItemTimeLine";
import { MdCheckCircle } from "react-icons/md";
import { motion } from "framer-motion";
import { useSectionVisibility } from "../hooks/useSectionVisibility";

export default function Education({ educationSectionRef }) {
  const isVisible = useSectionVisibility(educationSectionRef);

  return (
    <Container
      as={motion.div}
      maxW={"4xl"}
      ref={educationSectionRef}
      marginBottom={{ base: 10, md: 20 }}
      initial={{ opacity: 0 }}
      animate={{
        opacity: isVisible ? 1 : 0,
        transition: { duration: 1, delay: 0.5 },
      }}
      position="relative"
      overflow="hidden"
    >
      <HeaderSection number={"02"} label={"Education"} />
      <Flex direction={{ base: "column", md: "row" }}>
        <VStack align="stretch" spacing={8}>
          <ItemTimeLine
            year="2022"
            title="Web and mobile web developer"
            subtitle="Campus26/Simplon, Le Puy-en-Velay"
            technologies={[
              "HTML",
              "CSS",
              "JavaScript",
              "PHP",
              "MySQL",
              "Symfony",
              "React",
              "React Native",
              "Git",
              "GitLab",
            ]}
          >
            <List>
              <ListItem>
                <ListIcon
                  as={MdCheckCircle}
                  color="var(--bg-red)"
                  marginBottom={1}
                />
                Learning web programming languages (HTML/CSS, JS, PHP, MySQL)
                and frameworks (React & Symfony).
              </ListItem>
              <ListItem>
                <ListIcon
                  as={MdCheckCircle}
                  color="var(--bg-red)"
                  marginBottom={1}
                />
                Create static, dynamic and adaptable web user interfaces or with
                a content management or e-commerce solution.
              </ListItem>
              <ListItem>
                <ListIcon
                  as={MdCheckCircle}
                  color="var(--bg-red)"
                  marginBottom={1}
                />
                Create a database and develop data access components.
              </ListItem>
              <ListItem>
                <ListIcon
                  as={MdCheckCircle}
                  color="var(--bg-red)"
                  marginBottom={1}
                />
                Develop the back-end part of a web or mobile web application by
                processing, collecting and restoring new or existing data.
              </ListItem>
            </List>
          </ItemTimeLine>
          <ItemTimeLine
            year="2019"
            title="Master of Chemistry, environment option"
            subtitle="Université Savoie Mont Blanc, Chambéry"
          ></ItemTimeLine>
          <ItemTimeLine
            year="2016"
            title="Bachelor of Chemistry"
            subtitle="Université Savoie Mont Blanc, Chambéry"
          ></ItemTimeLine>
        </VStack>
        <Box
          position="absolute"
          left={{ base: "40px", md: "45px" }}
          top="90px"
          bottom="0"
          width="2px"
          bg="var(--bg-red)"
          zIndex={-1}
        />
      </Flex>
    </Container>
  );
}
