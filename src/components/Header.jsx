import {
  Heading,
  Container,
  Text,
  Button,
  Flex,
  useColorMode,
} from "@chakra-ui/react";
import { FaLinkedin } from "react-icons/fa";
import { motion } from "framer-motion";
import { useSectionVisibility } from "../hooks/useSectionVisibility";

export default function Header({ headerSectionRef, contactSectionRef }) {
  const { colorMode } = useColorMode();
  const isVisible = useSectionVisibility(headerSectionRef);

  const scrollToContact = () => {
    contactSectionRef.current.scrollIntoView({ behavior: "smooth" });
  };

  const handleClickLinkedin = () => {
    window.open("https://www.linkedin.com/in/guillaume-sepulveda/", "_blank");
  };

  return (
    <Container
      ref={headerSectionRef}
      maxWidth={"100%"}
      minHeight={"100vh"}
      paddingTop={"64px"}
    >
      <Flex
        as={motion.div}
        direction={"column"}
        align={"center"}
        justify={"center"}
        minHeight={"calc(100vh - 64px)"}
        textAlign={"center"}
        initial={{ opacity: 0, scale: 0.5 }}
        animate={{
          opacity: isVisible ? 1 : 0,
          scale: isVisible ? 1 : 0.5,
          transition: {
            duration: 1,
            delay: 0.2,
          },
        }}
      >
        <Heading
          as={motion.h1}
          size="4xl"
          fontSize={{ base: "4xl", md: "5xl", lg: "6xl" }}
          marginBottom={4}
          initial={{ y: 20 }}
          animate={{
            y: isVisible ? 0 : 20,
            transition: { duration: 1, delay: 0.8 },
          }}
        >
          Hello, my name is <strong>Guillaume</strong>
        </Heading>
        <Heading
          as={motion.h1}
          size="4xl"
          fontSize={{ base: "4xl", md: "5xl", lg: "6xl" }}
          color="var(--txt-red)"
          marginBottom={8}
          initial={{ opacity: 0, y: -20 }}
          animate={{
            opacity: isVisible ? 1 : 0,
            y: isVisible ? 0 : -20,
            transition: { duration: 1, delay: 0.8 },
          }}
        >
          I&apos;m a web developer
        </Heading>
        <Text
          color={
            colorMode === "light"
              ? "var(--txt-gray-dark)"
              : "var(--txt-gray-light)"
          }
          fontSize={{ base: "lg", sm: "xl", md: "2xl" }}
          maxWidth={{ base: "100%", sm: "100%", md: "80%", lg: "60%" }}
          marginBottom={8}
        >
          I build web applications with <strong>React</strong>,{" "}
          <strong>Vue.js</strong>, and <strong>Node</strong>. I&apos;m
          passionate about creating products that are both user-friendly and
          visually appealing.
        </Text>
        <Flex direction={"column"} align={"center"} justify={"center"}>
          <Button
            variant={"solid"}
            size={{ base: "md", sm: "lg" }}
            colorScheme="linkedin"
            marginBottom={4}
            onClick={handleClickLinkedin}
            leftIcon={<FaLinkedin />}
          >
            Connect with me !
          </Button>
          <Button
            variant={"link"}
            size={{ base: "md", sm: "lg" }}
            color={"var(--txt-red)"}
            marginBottom={4}
            onClick={scrollToContact}
          >
            Contact Me
          </Button>
        </Flex>
      </Flex>
    </Container>
  );
}
