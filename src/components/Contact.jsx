import { useState } from "react";
import {
  Text,
  Container,
  Heading,
  Icon,
  Box,
  Input,
  Textarea,
  Button,
  FormControl,
  FormLabel,
} from "@chakra-ui/react";
import { FaLinkedin, FaEnvelope, FaGitlab } from "react-icons/fa";
import HeaderSection from "./HeaderSection";
import axios from "axios";
import { motion } from "framer-motion";
import { useSectionVisibility } from "../hooks/useSectionVisibility";

export default function Contact({ contactSectionRef }) {
  const isVisible = useSectionVisibility(contactSectionRef);

  const profile = {
    email: "sepulveda.guillaume@gmail.com",
    phone: "+33 7 60 08 59 76",
  };

  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    company: "",
    email: "",
    message: "",
  });

  const [errorMessage, setErrorMessage] = useState(null);
  const [successMessage, setSuccessMessage] = useState(null);
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    sendContactForm();
  };

  const sendContactForm = async () => {
    setIsSubmitting(true);
    setErrorMessage(null);
    setSuccessMessage(null);
    try {
      if (
        !formData.firstName.trim() ||
        !formData.lastName.trim() ||
        !formData.email.trim() ||
        !formData.message.trim()
      ) {
        setErrorMessage("Please fill out all required fields.");
        return;
      }

      const response = await axios.post(
        `${import.meta.env.VITE_URL_API}/contact`,
        formData,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      if (response.status === 201) {
        setSuccessMessage(response.data.message);
        setFormData({
          firstName: "",
          lastName: "",
          company: "",
          email: "",
          message: "",
        });
      }
    } catch (error) {
      console.log(error);
      setErrorMessage(error.message);
    } finally {
      setIsSubmitting(false);
    }
  };

  const handleClickLinkedin = () => {
    window.open("https://www.linkedin.com/in/guillaume-sepulveda/", "_blank");
  };

  const handleClickGitLab = () => {
    window.open("https://gitlab.com/sepulveda.guillaume", "_blank");
  };

  const handleClickEmail = () => {
    window.open("mailto:sepulveda.guillaume@gmail.com");
  };

  return (
    <>
      <Container
        as={motion.div}
        maxW={"4xl"}
        ref={contactSectionRef}
        marginBottom={{ base: 10 }}
        initial={{ opacity: 0 }}
        animate={{
          opacity: isVisible ? 1 : 0,
          transition: { duration: 1, delay: 0.5 },
        }}
      >
        <HeaderSection number={"05"} label={"Contact"} />
        <Box textAlign={"center"}>
          <Heading fontSize={"3xl"} marginBottom={6} color={"var(--bg-red)"}>
            Let&apos;s get in touch
          </Heading>
          <Text fontWeight={"bold"} fontSize={"lg"} px={4} marginBottom={2}>
            {profile.email}
          </Text>
          <Text fontWeight={"bold"} fontSize={"lg"} px={4} marginBottom={2}>
            {profile.phone}
          </Text>
          <Box pt={4}>
            <Icon
              as={FaLinkedin}
              boxSize={10}
              marginRight={4}
              color="blue.400"
              _hover={{ color: "blue.300" }}
              onClick={handleClickLinkedin}
              cursor={"pointer"}
            />
            <Icon
              as={FaGitlab}
              boxSize={10}
              marginRight={4}
              color="orange.500"
              _hover={{ color: "orange.400" }}
              onClick={handleClickGitLab}
              cursor={"pointer"}
            />
            <Icon
              as={FaEnvelope}
              boxSize={10}
              marginRight={4}
              color="gray.400"
              _hover={{ color: "gray.300" }}
              onClick={handleClickEmail}
              cursor={"pointer"}
            />
          </Box>
          <Box mt={10}>
            <form onSubmit={handleSubmit}>
              <FormControl id="firstName" isRequired>
                <FormLabel>First Name</FormLabel>
                <Input
                  type="text"
                  name="firstName"
                  value={formData.firstName}
                  onChange={handleChange}
                />
              </FormControl>
              <FormControl id="lastName" isRequired mt={4}>
                <FormLabel>Last Name</FormLabel>
                <Input
                  type="text"
                  name="lastName"
                  value={formData.lastName}
                  onChange={handleChange}
                />
              </FormControl>
              <FormControl id="company" mt={4}>
                <FormLabel>Company</FormLabel>
                <Input
                  type="text"
                  name="company"
                  value={formData.company}
                  onChange={handleChange}
                />
              </FormControl>
              <FormControl id="email" isRequired mt={4}>
                <FormLabel>Email</FormLabel>
                <Input
                  type="email"
                  name="email"
                  value={formData.email}
                  onChange={handleChange}
                />
              </FormControl>
              <FormControl id="message" isRequired mt={4}>
                <FormLabel>Message</FormLabel>
                <Textarea
                  name="message"
                  value={formData.message}
                  onChange={handleChange}
                />
              </FormControl>
              <Button
                type="submit"
                isLoading={isSubmitting}
                bgColor={"var(--bg-red)"}
                _hover={{ bgColor: "var(--bg-red-hover)" }}
                mt={6}
              >
                Submit
              </Button>
            </form>
            {isSubmitting && (
              <Text color={"blue.500"} mt={4}>
                Sending message...
              </Text>
            )}
            {errorMessage && (
              <Text color={"red.500"} mt={4}>
                {errorMessage}
              </Text>
            )}
            {successMessage && (
              <Text color={"green.500"} mt={4}>
                {successMessage}
              </Text>
            )}
          </Box>
        </Box>
      </Container>
    </>
  );
}
