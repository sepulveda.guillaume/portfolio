import {
  Container,
  Flex,
  VStack,
  List,
  ListItem,
  ListIcon,
  Box,
} from "@chakra-ui/react";
import HeaderSection from "./HeaderSection";
import ItemTimeLine from "./ItemTimeLine";
import { MdCheckCircle } from "react-icons/md";
import { motion } from "framer-motion";
import { useSectionVisibility } from "../hooks/useSectionVisibility";

export default function Experience({ experienceSectionRef }) {
  const isVisible = useSectionVisibility(experienceSectionRef);

  return (
    <Container
      as={motion.div}
      maxW={"4xl"}
      ref={experienceSectionRef}
      marginBottom={{ base: 10, md: 20 }}
      position="relative"
      overflow="hidden"
      initial={{ opacity: 0 }}
      animate={{
        opacity: isVisible ? 1 : 0,
        transition: { duration: 1, delay: 0.5 },
      }}
    >
      <HeaderSection number={"03"} label={"Experience"} />
      <Flex direction={{ base: "column", md: "row" }}>
        <VStack align="stretch" spacing={8}>
          <ItemTimeLine
            year="2024"
            title="Personal projects"
            subtitle="Remote"
            technologies={[
              "HTML",
              "CSS",
              "JavaScript",
              "React",
              "Node",
              "Express",
              "MongoDB",
              "Firebase",
              "Redis",
              "WS",
              "Git",
              "GitLab",
            ]}
          >
            <List>
              <ListItem>
                <ListIcon
                  as={MdCheckCircle}
                  color="var(--bg-red)"
                  marginBottom={1}
                />
                Development of a multiplayer blind test via a ws, ability to add
                and download music from the Spotify and Youtube API.
              </ListItem>
              <ListItem>
                <ListIcon
                  as={MdCheckCircle}
                  color="var(--bg-red)"
                  marginBottom={1}
                />
                Development of a multiplayer quiz via a ws, retrieval of
                questions from an API and a database.
              </ListItem>
              <ListItem>
                <ListIcon
                  as={MdCheckCircle}
                  color="var(--bg-red)"
                  marginBottom={1}
                />
                Development of a five-letters Wordle game in React/Express from
                Scratch with a authentication system to have some stats.
              </ListItem>
            </List>
          </ItemTimeLine>
          <ItemTimeLine
            year="2023"
            title="Web developer"
            subtitle="CYIM CyberImagination, Rennes"
            technologies={[
              "HTML",
              "CSS",
              "JavaScript",
              "Vue",
              "Node",
              "Express",
              "MongoDB",
              "GraphQL",
              "Firebase",
              "Apollo",
              "Git",
              "GitLab",
              "SQL Server",
            ]}
          >
            <List>
              <ListItem>
                <ListIcon
                  as={MdCheckCircle}
                  color="var(--bg-red)"
                  marginBottom={1}
                />
                Develop and design interfaces in accordance with the
                specifications and according to the SCRUM methodology.
              </ListItem>
              <ListItem>
                <ListIcon
                  as={MdCheckCircle}
                  color="var(--bg-red)"
                  marginBottom={1}
                />
                Control and provide the elements necessary for the successful
                completion of the project with the other divisions.
              </ListItem>
            </List>
          </ItemTimeLine>
          <ItemTimeLine
            year="2022"
            title="Web developer - Internship"
            subtitle="isiTecc, Saint-Just-Malmont"
            technologies={[
              "HTML",
              "CSS",
              "JavaScript",
              "ASP.NET",
              "Git",
              "GitLab",
              "SQL Server",
            ]}
          >
            -
            <List>
              <ListItem>
                <ListIcon
                  as={MdCheckCircle}
                  color="var(--bg-red)"
                  marginBottom={1}
                />
                Creation of several web screens allowing the monitoring of the
                production of aluminum threading for a specialized company.
              </ListItem>
              <ListItem>
                <ListIcon
                  as={MdCheckCircle}
                  color="var(--bg-red)"
                  marginBottom={1}
                />
                Use of the ASP.NET MVC framework and the DevExpress control
                provider for web development.
              </ListItem>
            </List>
          </ItemTimeLine>
          <ItemTimeLine
            year="2020"
            title="Atmospheric sampling technician"
            subtitle="Québec, Canada"
          ></ItemTimeLine>
          <ItemTimeLine
            year="2019"
            title="Apprentice in charge of studies and works manager on polluted sites and soils"
            subtitle="GAUTHEY, La Ravoire"
          ></ItemTimeLine>
        </VStack>
        <Box
          position="absolute"
          left={{ base: "40px", md: "45px" }}
          top="90px"
          bottom="0"
          width="2px"
          bg="var(--bg-red)"
          zIndex={-1}
        />
      </Flex>
    </Container>
  );
}
