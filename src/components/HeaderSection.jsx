import { Divider, Flex, Text } from "@chakra-ui/react";

export default function HeaderSection({ number, label }) {
  return (
    <Flex
      alignItems="center"
      justifyContent="center"
      mb={8}
      fontSize={{ base: "2xl", md: "4xl" }}
    >
      <Text fontWeight={"bold"} mr={3}>
        {number}.
      </Text>
      <Text fontWeight={"bold"} mr={5}>
        {label}
      </Text>
      <Divider orientation="horizontal" opacity={1} />
    </Flex>
  );
}
