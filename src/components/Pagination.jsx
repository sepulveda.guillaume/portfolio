import { Button, ButtonGroup } from "@chakra-ui/react";
import { useState } from "react";

export default function Pagination({ currentPage, totalPages, onPageChange }) {
  const [pagesToShow] = useState(5);
  const [startPage, setStartPage] = useState(1);

  const handlePageChange = (page) => {
    onPageChange(page);
    if (page > startPage + pagesToShow - 1) {
      setStartPage(page - pagesToShow + 1);
    }
    if (page < startPage) {
      setStartPage(page);
    }
  };

  const renderPages = () => {
    const pages = [];
    for (let i = startPage; i <= Math.min(totalPages, startPage + pagesToShow - 1); i++) {
      pages.push(
        <Button
          key={i}
          onClick={() => handlePageChange(i)}
          variant={currentPage === i ? "solid" : "outline"}
          bgColor={currentPage === i ? "var(--bg-red)" : "var(--bg-gray)"}
          _hover={{
            bgColor: "var(--bg-red-hover)",
          }}
        >
          {i}
        </Button>
      );
    }
    return pages;
  };

  return (
    <ButtonGroup spacing={2} isAttached variant="outline">
      <Button
        onClick={() => handlePageChange(currentPage - 1)}
        isDisabled={currentPage === 1}
      >
        Prev
      </Button>
      {renderPages()}
      <Button
        onClick={() => handlePageChange(currentPage + 1)}
        isDisabled={currentPage === totalPages}
      >
        Next
      </Button>
    </ButtonGroup>
  );
}
