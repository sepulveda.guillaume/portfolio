import { Container, Text, useColorMode } from "@chakra-ui/react";

export default function Footer() {
  const { colorMode } = useColorMode();
  return (
    <Container maxW={"4xl"} marginBottom={4}>
      <Text
        textAlign="center"
        fontSize={{ base: "sm", md: "md" }}
        marginTop={10}
        color={
          colorMode === "light"
            ? "var(--txt-gray-dark)"
            : "var(--txt-gray-light)"
        }
      >
        © 2023 Guillaume Sepulveda. All rights reserved
      </Text>
    </Container>
  );
}
