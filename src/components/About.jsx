import { Text, Container, Box, useColorMode, Image } from "@chakra-ui/react";
import HeaderSection from "./HeaderSection";
import { motion } from "framer-motion";
import { useSectionVisibility } from "../hooks/useSectionVisibility";

export default function About({ aboutSectionRef }) {
  const { colorMode } = useColorMode();
  const isVisible = useSectionVisibility(aboutSectionRef);

  return (
    <Container
      maxW={"4xl"}
      ref={aboutSectionRef}
      marginBottom={{ base: 10, md: 20 }}
    >
      <HeaderSection number={"01"} label={"About"} />
      <Box
        as={motion.div}
        display={{ base: "block" }}
        textAlign={"justify"}
        initial={{ opacity: 0 }}
        animate={{
          opacity: isVisible ? 1 : 0,
          transition: { duration: 1, delay: 0.5 },
        }}
      >
        <Image
          src="/guillaume.jpg"
          alt="Guillaume Sepulveda"
          borderRadius="md"
          maxW={{ base: "100%", md: "48%" }}
          float={"right"}
          ml={8}
          mb={{ base: 4, md: 0 }}
        />
        <Text
          color={
            colorMode === "light"
              ? "var(--txt-gray-dark)"
              : "var(--txt-gray-light)"
          }
          fontSize={{ base: "lg", md: "xl" }}
          lineHeight={1.8}
        >
          Hey there! I&apos;m <span className="highlight">Guillaume</span>. 👋{" "}
          <br />I dove headfirst into the world of web and mobile development
          through a formation at{" "}
          <span className="highlight">Campus26/Simplon</span>, nestled in the
          heart of Puy-en-Velay in 2021/2022. It was a bit like taking a giant
          leap into the digital realm! 🚀 This adventure equipped me with solid{" "}
          <span className="highlight">web programming skills</span>, all while
          honing my <span className="highlight">soft skills</span>. Next stop? I
          snagged a gig as a web developer at{" "}
          <span className="highlight">CYIM - Cyber Imagination</span>, based in
          Rennes. At CYIM, I was thrust into the congress department, where the
          pace was set to the frenetic beat of medical events. Picture juggling{" "}
          <span className="highlight">Vue.js, Node, and Express</span>, all
          while keeping up with the brisk cadence of{" "}
          <span className="highlight">Scrum and Agile</span>. 💻 I learned to
          dance to the rhythm of tight deadlines and{" "}
          <span className="highlight">fast-paced sprints</span> – all with a
          smile! 😅 Beyond the 9-to-5 grind, I&apos;m an adventure enthusiast!
          🌍 I strapped on my backpack for a{" "}
          <span className="highlight">four-month travel</span> across Southeast
          Asia: India, Nepal, Thailand, Laos... It was a true odyssey,
          encountering fascinating cultures and breathtaking landscapes. And
          guess what? I scaled <span className="highlight">mountains</span>,
          snorkeled into turquoise waters, and{" "}
          <span className="highlight">savored every moment </span>! 🏔️🏝️ But
          wait, there&apos;s more! 🎬 A nature and cinema buff, I have a soft
          spot for outdoor escapades and movie nights under the stars. And once
          I&apos;m back home, it&apos;s time to put my developer chops to work!
          I&apos;ve tinkered on{" "}
          <span className="highlight">React and Node </span>
          projects, including a{" "}
          <span className="highlight">multiplayer quiz with WSs</span>, a{" "}
          <span className="highlight">blindtest</span> that pulls tracks from
          YouTube or Spotify, and even a{" "}
          <span className="highlight">Wordle game</span>. In a nutshell,
          I&apos;m a tech aficionado, adventure seeker, and bon vivant rolled
          into one – always eager to embark on the next big thing !
        </Text>
      </Box>
    </Container>
  );
}
