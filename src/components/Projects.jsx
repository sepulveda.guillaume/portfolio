import { useEffect, useState } from "react";
import {
  Flex,
  Text,
  Container,
  Box,
  Button,
  ButtonGroup,
  Card,
  CardBody,
  Image,
  Heading,
  SimpleGrid,
  useColorMode,
  Tooltip,
} from "@chakra-ui/react";
import HeaderSection from "./HeaderSection";
import Pagination from "./Pagination";
import projects from "../projects";
import { useTechnologyIcon } from "../hooks/useTechnologyIcon";
import { motion } from "framer-motion";
import { useSectionVisibility } from "../hooks/useSectionVisibility";

export default function Projects({ projectsSectionRef }) {
  const isVisible = useSectionVisibility(projectsSectionRef);
  const projectsPerPage = 3;
  const { getTechnologyIcon } = useTechnologyIcon();
  const { colorMode } = useColorMode();

  const [selected, setSelected] = useState("All");
  const [currentPage, setCurrentPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [filteredProjects, setFilteredProjects] = useState([]);

  useEffect(() => {
    setLoading(true);
    const filtered =
      selected === "All"
        ? projects
        : projects.filter((project) => project.badges.includes(selected));

    setTimeout(() => {
      setFilteredProjects(filtered);
      setLoading(false);
    }, 500);
  }, [selected]);

  const totalPages = Math.ceil(filteredProjects.length / projectsPerPage);

  const handleSelected = (value) => {
    setSelected(value);
    setCurrentPage(1);
  };

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  return (
    <Container
      as={motion.div}
      maxW={"4xl"}
      ref={projectsSectionRef}
      marginBottom={{ base: 10, md: 20 }}
      initial={{ opacity: 0 }}
      animate={{
        opacity: isVisible ? 1 : 0,
        transition: { duration: 1, delay: 0.5 },
      }}
    >
      <HeaderSection number={"04"} label={"Projects"} />
      <Box textAlign={"center"} spacing={{ base: 8, md: 14 }}>
        <ButtonGroup variant="outline" flexWrap={"wrap"}>
          {[
            "All",
            ...new Set(projects.flatMap((project) => project.badges)),
          ].map((option, index) => (
            <Button
              as={motion.button}
              whileTap={{ scale: 0.95, transition: { duration: 0.1 } }}
              key={index}
              onClick={() => handleSelected(option)}
              isActive={selected === option}
              bgColor={selected === option && "var(--bg-red) !important"}
              marginBottom={{ base: 2, md: 0 }}
              _hover={{
                bgColor: "var(--bg-red)",
              }}
            >
              {option}
            </Button>
          ))}
        </ButtonGroup>
        <SimpleGrid
          columns={{ base: 1, sm: 3 }}
          spacing={4}
          py={6}
          as={motion.div}
          initial={{ opacity: 0 }}
          animate={{
            opacity: !loading ? 1 : 0,
            transition: { duration: 0.5 },
          }}
        >
          {filteredProjects
            .slice(
              (currentPage - 1) * projectsPerPage,
              currentPage * projectsPerPage
            )
            .map((project, index) => (
              <Card
                as={motion.div}
                whileHover={{ scale: 1.05 }}
                transition={{ duration: 0.3 }}
                key={index}
                overflow="hidden"
                bgColor="transparent"
                border="2px"
                boxShadow={"lg"}
                borderColor={
                  colorMode === "light"
                    ? "var(--txt-gray-light)"
                    : "var(--txt-gray-dark)"
                }
              >
                <Flex
                  direction="column"
                  align="stretch"
                  justify="space-between"
                  height="100%"
                >
                  <CardBody align="justify">
                    <Image
                      objectFit="cover"
                      src={project.image}
                      marginBottom={4}
                    />
                    <Heading size="md">{project.name}</Heading>
                    <Text
                      py={2}
                      color={
                        colorMode === "light"
                          ? "var(--txt-gray-dark)"
                          : "var(--txt-gray-light)"
                      }
                    >
                      {project.description}
                    </Text>
                    <Flex pt={2} pb={2}>
                      {project.buttons.map((button, index) => (
                        <a
                          key={index}
                          href={button.href}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          <Button
                            marginRight={4}
                            _hover={{ bgColor: "var(--bg-red) !important" }}
                          >
                            {button.text}
                          </Button>
                        </a>
                      ))}
                    </Flex>
                    <Flex pt={2}>
                      {project.badges.map((badge, index) => (
                        <Tooltip key={index} label={badge} aria-label={badge}>
                          <Box marginRight={2} key={index}>
                            {getTechnologyIcon(badge)}
                          </Box>
                        </Tooltip>
                      ))}
                    </Flex>
                  </CardBody>
                </Flex>
              </Card>
            ))}
        </SimpleGrid>
        <Pagination
          currentPage={currentPage}
          totalPages={totalPages}
          onPageChange={paginate}
        />
      </Box>
    </Container>
  );
}
