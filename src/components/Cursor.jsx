import { useColorMode } from "@chakra-ui/react";
import { motion } from "framer-motion";
import { useEffect, useState } from "react";

export default function Cursor() {
  const { colorMode } = useColorMode();
  const [position, setPosition] = useState({ x: 0, y: 0 });

  const updateCursorPosition = (e) => {
    setPosition({ x: e.clientX, y: e.clientY });
  };

  useEffect(() => {
    window.addEventListener("mousemove", updateCursorPosition);

    return () => {
      window.removeEventListener("mousemove", updateCursorPosition);
    };
  }, []);

  return (
    <motion.div
      style={{
        position: "fixed",
        top: position.y,
        left: position.x,
        pointerEvents: "none",
        zIndex: 9999,
      }}
    >
      <motion.div
        style={{
          width: 80,
          height: 80,
          borderRadius: "50%",
          backgroundColor:
            colorMode === "light"
              ? "rgba(255, 0, 0, 0.3)"
              : "rgba(255, 255, 255, 0.3)",
          position: "absolute",
          translateX: "-50%",
          translateY: "-50%",
        }}
        initial={{ scale: 0 }}
        animate={{ scale: 1 }}
        transition={{ duration: 0.1 }}
        
      />
    </motion.div>
  );
}
