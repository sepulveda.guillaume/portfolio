import { Button, Flex, useColorMode, IconButton } from "@chakra-ui/react";
import { MoonIcon, SunIcon, HamburgerIcon, CloseIcon } from "@chakra-ui/icons";
import { useState } from "react";

export default function NavBar({ sectionRefs }) {
  const { colorMode, toggleColorMode } = useColorMode();
  const [showMenu, setShowMenu] = useState(false);

  const handleScrollToSection = (ref) => {
    const yOffset = -100;
    const y =
      ref.current.getBoundingClientRect().top + window.pageYOffset + yOffset;
    window.scrollTo({ top: y, behavior: "smooth" });
    setShowMenu(false);
  };

  return (
    <Flex
      position={"fixed"}
      alignItems={{ md: "center" }}
      justifyContent={{ base: "space-between", md: "flex-end" }}
      top={0}
      right={0}
      zIndex={1000}
      minWidth={"100%"}
      bgColor={colorMode === "light" ? "var(--bg-white)" : "var(--bg-main)"}
      padding={2}
    >
      <IconButton
        display={{ base: "block", md: "none" }}
        aria-label="Open menu"
        icon={showMenu ? <CloseIcon /> : <HamburgerIcon />}
        onClick={() => setShowMenu(!showMenu)}
        marginRight={2}
      />
      <Flex
        display={{ base: showMenu ? "flex" : "none", md: "flex" }}
        wrap={"wrap"}
        direction={"row"}
        alignItems="center"
        marginRight={{ base: 0, md: showMenu ? 2 : 0 }}
      >
        {Object.entries(sectionRefs).map(([sectionName, sectionInfo]) => (
          <Button
            key={sectionName}
            variant={"ghost"}
            size={{ base: "sm", md: "md" }}
            _hover={{ bg: "var(--bg-red)" }}
            margin={{ base: 0, md: 2 }}
            onClick={() => handleScrollToSection(sectionInfo.ref)}
          >
            {sectionInfo.label}
          </Button>
        ))}
      </Flex>
      <Button
        onClick={toggleColorMode}
        width={45}
        height={45}
        marginRight={{ md: 1 }}
      >
        {colorMode === "light" ? <MoonIcon /> : <SunIcon />}
      </Button>
    </Flex>
  );
}
