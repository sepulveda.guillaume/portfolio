import {
  Circle,
  HStack,
  Stack,
  Text,
  VStack,
  useColorMode,
  Tooltip,
} from "@chakra-ui/react";
import { useTechnologyIcon } from "../hooks/useTechnologyIcon";

export default function ItemTimeLine({
  year,
  title,
  subtitle,
  technologies = [],
  children,
}) {
  const { colorMode } = useColorMode();
  const { getTechnologyIcon } = useTechnologyIcon();

  return (
    <HStack spacing={4} align="flex-start">
      <Circle
        size={{ base: "50px", md: "60px" }}
        bg="var(--bg-red)"
        color="var(--txt-main)"
        textAlign="center"
        fontSize={{ base: "lg", md: "xl" }}
      >
        {year}
      </Circle>
      <VStack align="flex-start" fontSize={{ base: "lg", md: "xl" }}>
        <Text fontWeight={"bold"}>{title}</Text>
        <Text>{subtitle}</Text>
        {technologies.length > 0 && (
          <HStack spacing={2} mt={2} wrap={"wrap"}>
            {technologies.map((tech, index) => (
              <Tooltip key={index} label={tech} aria-label={tech}>
                <span>{getTechnologyIcon(tech)}</span>
              </Tooltip>
            ))}
          </HStack>
        )}
        <Stack
          spacing={3}
          marginTop={2}
          color={
            colorMode === "light"
              ? "var(--txt-gray-dark)"
              : "var(--txt-gray-light)"
          }
          lineHeight={1.8}
          fontSize={{ base: "md", md: "lg" }}
          textAlign={"justify"}
        >
          {children}
        </Stack>
      </VStack>
    </HStack>
  );
}
