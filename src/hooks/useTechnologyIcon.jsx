import {
  FaHtml5,
  FaCss3Alt,
  FaJs,
  FaReact,
  FaPhp,
  FaNodeJs,
  FaVuejs,
  FaGitlab,
  FaSymfony,
  FaGitAlt,
} from "react-icons/fa";
import {
  SiGraphql,
  SiApollographql,
  SiDotnet,
  SiExpress,
  SiMongodb,
  SiMysql,
  SiMicrosoftsqlserver,
  SiRedis,
  SiSocketdotio,
  SiFirebase,
} from "react-icons/si";
import { Icon } from "@chakra-ui/react";
import { TbBrandReactNative } from "react-icons/tb";
import { useColorMode } from "@chakra-ui/react";

export const useTechnologyIcon = () => {
  const { colorMode } = useColorMode();

  const getTechnologyIcon = (technologyName) => {
    switch (technologyName) {
      case "HTML":
        return <Icon as={FaHtml5} boxSize={8} color="orange.500" />;
      case "CSS":
        return <Icon as={FaCss3Alt} boxSize={8} color="blue.400" />;
      case "JavaScript":
        return <Icon as={FaJs} boxSize={8} color="yellow.400" />;
      case "React":
        return <Icon as={FaReact} boxSize={8} color="cyan.400" />;
      case "PHP":
        return <Icon as={FaPhp} boxSize={8} color="purple.400" />;
      case "Node":
        return <Icon as={FaNodeJs} boxSize={8} color="green.400" />;
      case "Vue":
        return <Icon as={FaVuejs} boxSize={8} color="green.400" />;
      case "ASP.NET":
        return <Icon as={SiDotnet} boxSize={8} color="blue.400" />;
      case "Symfony":
        return (
          <Icon
            as={FaSymfony}
            boxSize={8}
            color={
              colorMode === "light"
                ? "var(--txt-gray-dark)"
                : "var(--txt-gray-light"
            }
          />
        );
      case "Git":
        return <Icon as={FaGitAlt} boxSize={8} color="orange.600" />;
      case "GitLab":
        return <Icon as={FaGitlab} boxSize={8} color="orange.500" />;
      case "MySQL":
        return <Icon as={SiMysql} boxSize={8} color="blue.400" />;
      case "MongoDB":
        return <Icon as={SiMongodb} boxSize={8} color="green.400" />;
      case "GraphQL":
        return <Icon as={SiGraphql} boxSize={8} color="pink.400" />;
      case "Apollo":
        return <Icon as={SiApollographql} boxSize={8} color="purple.400" />;
      case "SQL Server":
        return <Icon as={SiMicrosoftsqlserver} boxSize={8} color="red.500" />;
      case "Express":
        return (
          <Icon
            as={SiExpress}
            boxSize={8}
            color={
              colorMode === "light"
                ? "var(--txt-gray-dark)"
                : "var(--txt-gray-light"
            }
          />
        );
      case "Redis":
        return <Icon as={SiRedis} boxSize={8} color="red.500" />;
      case "WS":
        return (
          <Icon
            as={SiSocketdotio}
            boxSize={8}
            color={
              colorMode === "light"
                ? "var(--txt-gray-dark)"
                : "var(--txt-gray-light"
            }
          />
        );
      case "Firebase":
        return <Icon as={SiFirebase} boxSize={8} color="yellow.500" />;
      case "React Native":
        return <Icon as={TbBrandReactNative} boxSize={8} color="cyan.400" />;
      default:
        return null;
    }
  };

  return { getTechnologyIcon };
};
