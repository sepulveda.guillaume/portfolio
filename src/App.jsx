import NavBar from "./components/NavBar";
import Header from "./components/Header";
import About from "./components/About";
import Experience from "./components/Experience";
import Projects from "./components/Projects";
import Contact from "./components/Contact";
import Footer from "./components/Footer";
import { useRef } from "react";
import Education from "./components/Education";
import Cursor from "./components/Cursor";

export default function App() {
  const sectionRefs = {
    headerSectionRef: { ref: useRef(null), label: "Home" },
    aboutSectionRef: { ref: useRef(null), label: "About" },
    educationSectionRef: { ref: useRef(null), label: "Education" },
    experienceSectionRef: { ref: useRef(null), label: "Experience" },
    projectsSectionRef: { ref: useRef(null), label: "Projects" },
    contactSectionRef: { ref: useRef(null), label: "Contact" },
  };

  return (
    <>
      <Cursor />
      <NavBar sectionRefs={sectionRefs} />
      <Header
        headerSectionRef={sectionRefs.headerSectionRef.ref}
        contactSectionRef={sectionRefs.contactSectionRef.ref}
      />
      <About aboutSectionRef={sectionRefs.aboutSectionRef.ref} />
      <Education educationSectionRef={sectionRefs.educationSectionRef.ref} />
      <Experience experienceSectionRef={sectionRefs.experienceSectionRef.ref} />
      <Projects projectsSectionRef={sectionRefs.projectsSectionRef.ref} />
      <Contact contactSectionRef={sectionRefs.contactSectionRef.ref} />
      <Footer />
    </>
  );
}
