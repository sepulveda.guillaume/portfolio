const projects = [
  {
    name: "La Guerre des Mélodies",
    description:
      "Development of a multiplayer blind test via a ws, ability to add and download music from the Spotify API and convert into mp3 from Youtube API.",
    image: "/projects/la_guerre_des-melodies.png",
    buttons: [
      {
        text: "View",
        href: "https://la-guerre-des-melodies.vercel.app",
      },
      {
        text: "Source",
        href: "https://gitlab.com/sepulveda.guillaume/la-guerre-des-melodies",
      },
    ],
    badges: ["React", "Node", "Express", "MongoDB", "WS"],
  },
  {
    name: "BrainBrawl",
    description:
      "Development of a multiplayer quiz game via a ws and retrieval of questions from an API Rest. Storage the scores in a MongoDB database.",
    image: "/projects/brain_brawl.png",
    buttons: [
      {
        text: "View",
        href: "https://brainbrawl-liart.vercel.app",
      },
      {
        text: "Source",
        href: "https://gitlab.com/sepulveda.guillaume/brainbrawl",
      },
    ],
    badges: ["React", "Node", "Express", "MongoDB", "WS"],
  },
  {
    name: "Wordle",
    description:
      "Development of a five-letters Wordle game in React/Express with a authentication system and Google Provider to have some stats.",
    image: "/projects/wordle.png",
    buttons: [
      {
        text: "View",
        href: "https://wordle-five-letters.vercel.app",
      },
      {
        text: "Source",
        href: "https://gitlab.com/sepulveda.guillaume/wordle",
      },
    ],
    badges: ["React", "Node", "Express", "MongoDB", "Firebase"],
  },
  {
    name: "RichardFlix",
    description:
      "This project is a web application that allows users to view a list of popular movies and view detailed information about these movies. The design is inspired by Netflix.",
    image: "/projects/richardflix.png",
    buttons: [
      {
        text: "View",
        href: "https://richardflix.vercel.app",
      },
      {
        text: "Source",
        href: "https://gitlab.com/sepulveda.guillaume/richard-et-lucy-les-cinephiles-en-folie",
      },
    ],
    badges: ["React", "Node", "Express", "MongoDB", "Redis"],
  },
  {
    name: "PeakConnect",
    description:
      "Development of a social network (clone of IG) for mountaineers. The user can create an account, add picture, like and comment some pictures. (In progress)",
    image: "/projects/peak_connect.png",
    buttons: [
      {
        text: "Source",
        href: "https://gitlab.com/sepulveda.guillaume/peakconnect",
      },
    ],
    badges: ["React", "Node", "Express", "MongoDB"],
  },
  {
    name: "Chope-moi",
    description:
      "Development of a mobile application in RN to have informations about beers after scanning the barcode. The user car add some beers in his favorites.",
    image: "/projects/chope_moi.png",
    buttons: [
      {
        text: "Source",
        href: "https://gitlab.com/sepulveda.guillaume/chope-moi",
      },
    ],
    badges: ["React Native", "Firebase"],
  },
];

export default projects;
