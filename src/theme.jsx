import { extendTheme } from "@chakra-ui/react";

const config = {
  initialColorMode: "dark",
  useSystemColorMode: false,
};

const theme = extendTheme({
  config,
  styles: {
    global: ({ colorMode }) => ({
      body: {
        bg: colorMode === "light" ? "var(--bg-white)" : "var(--bg-main)",
      },
    }),
  },
});


export { theme };
