const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const nodemailer = require("nodemailer");

require("dotenv").config();

const app = express();

const port = 3000;

mongoose
  .connect(process.env.MANGOOSE_CONNECTION)
  .then(() => console.log("Connexion à MongoDB réussie !"))
  .catch(() => console.log("Connexion à MongoDB échouée !"));

app.use(cors());
app.use(express.json());

const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 587,
  secure: false,
  auth: {
    user: process.env.GMAIL_SMTP_USER,
    pass: process.env.GMAIL_SMTP_PASSWORD,
  },
});

// Mongoose Schema
const contactSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  company: {
    type: String,
  },
  email: {
    type: String,
    required: true,
  },
  message: {
    type: String,
    required: true,
  },
});

const Contact = mongoose.model("Contact", contactSchema);

// API routes
app.post("/api/contact", async (req, res) => {
  const { firstName, lastName, company, email, message } = req.body;

  // Vérifier que tous les champs requis sont remplis
  if (!firstName || !lastName || !email || !message) {
    return res.status(400).json({
      error: "Please fill in all required fields",
    });
  }

  // Sauvegarder le message dans la base de données
  try {
    const newContact = new Contact({
      firstName,
      lastName,
      company,
      email,
      message,
    });
    await newContact.save();
  } catch (error) {
    return res.status(500).json({
      error: "Your message could not be saved, please try again later",
    });
  }

  try {
    await transporter.sendMail({
      from: process.env.GMAIL_SMTP_USER,
      to: process.env.GMAIL_SMTP_USER,
      subject: `Nouveau message de ${firstName} ${lastName} ${
        company && `(de la société ${company})`
      } via le formulaire de contact`,
      text: `Vous avez reçu un nouveau message de contact :\n\n
      Nom : ${firstName} ${lastName}\n
      Société : ${company}\n
      E-mail : ${email}\n
      Message : ${message}`,
    });
  } catch (error) {
    return res.status(500).json({
      error: "Your message could not be sent, please try again later",
    });
  }

  res.status(201).json({ message: "Your message has been sent successfully" });
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
